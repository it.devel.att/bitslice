package bitslice

import (
	"testing"
)

func Test_Len(t *testing.T) {
	type testCase struct {
		initialSize uint64
		expectedLen int
	}

	for _, tc := range []testCase{
		{
			initialSize: 10,
			expectedLen: 64,
		},
		{
			initialSize: 64,
			expectedLen: 64,
		},
		{
			initialSize: 65,
			expectedLen: 128,
		},
		{
			initialSize: 128,
			expectedLen: 128,
		},
		{
			initialSize: 129,
			expectedLen: 192,
		},
	} {
		t.Run("", func(t *testing.T) {
			s := NewBitSlice(tc.initialSize)
			actualLen := s.Len()

			if tc.expectedLen != actualLen {
				t.Errorf("expected len %v got %v", tc.expectedLen, actualLen)
			}
		})
	}
}

func Test_SetAndAt(t *testing.T) {
	type testCase struct {
		initialSize   uint64
		setAt         uint64
		checkAt       uint64
		expected      bool
		expectedPanic bool
	}

	for _, tc := range []testCase{
		{
			initialSize: 10,
			setAt:       13,
			checkAt:     13,
			expected:    true,
		},
		{
			initialSize: 10,
			setAt:       13,
			checkAt:     12,
			expected:    false,
		},
		{
			initialSize: 10,
			setAt:       0,
			checkAt:     0,
			expected:    true,
		},
		{
			initialSize:   10,
			setAt:         64,
			checkAt:       0,
			expectedPanic: true,
		},
	} {
		t.Run("", func(t *testing.T) {
			defer checkPanic(t, tc.expectedPanic)

			s := NewBitSlice(tc.initialSize)

			s.Set(tc.setAt)

			actualAt := s.At(tc.checkAt)
			if tc.expected != actualAt {
				t.Errorf("expected %v got %v", tc.expected, actualAt)
			}
			// check all other bits untouched
			for i := 0; i < s.Len(); i++ {
				if uint64(i) == tc.setAt {
					continue
				}

				if false != s.At(uint64(i)) {
					t.Errorf("position %v should be %v got %v", i, false, true)
				}
			}
		})
	}
}

func Test_UnsetAndAt(t *testing.T) {
	type testCase struct {
		initialSize   uint64
		unsetAt       uint64
		checkAt       uint64
		expected      bool
		expectedPanic bool
	}

	for _, tc := range []testCase{
		{
			initialSize: 10,
			unsetAt:     13,
			checkAt:     13,
			expected:    false,
		},
		{
			initialSize: 10,
			unsetAt:     13,
			checkAt:     12,
			expected:    true,
		},
		{
			initialSize: 10,
			unsetAt:     0,
			checkAt:     0,
			expected:    false,
		},
		{
			initialSize:   10,
			unsetAt:       64,
			checkAt:       0,
			expectedPanic: true,
		},
	} {
		t.Run("", func(t *testing.T) {
			defer checkPanic(t, tc.expectedPanic)

			s := NewBitSlice(tc.initialSize)

			// set all bits to 1
			for i := 0; i < s.Len(); i++ {
				s.Set(uint64(i))
			}
			s.Unset(tc.unsetAt)

			actualAt := s.At(tc.checkAt)
			if tc.expected != actualAt {
				t.Errorf("expected %v got %v", tc.expected, actualAt)
			}
			// check all other bits untouched
			for i := 0; i < s.Len(); i++ {
				if uint64(i) == tc.unsetAt {
					continue
				}

				if !s.At(uint64(i)) {
					t.Errorf("position %v should be %v got %v", i, true, false)
				}
			}
		})
	}
}

func checkPanic(t *testing.T, shouldPanic bool) {
	if r := recover(); r != nil {
		if !shouldPanic {
			t.Errorf("unexpected panic: %v", r)
			return
		}
	} else {
		if shouldPanic {
			t.Error("expected panic doesn't happen")
			return
		}
	}
}
