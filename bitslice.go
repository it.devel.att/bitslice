package bitslice

import (
	"math"
)

const positionsInSlot = 64

type slice []uint64

type BitSlice interface {
	At(i uint64) bool
	Set(i uint64)
	Unset(i uint64)
	Len() int
}

func NewBitSlice(size uint64) BitSlice {
	length := int(math.Ceil(float64(size) / float64(positionsInSlot)))
	if length == 0 {
		length = 1
	}
	return make(slice, length)
}

func (s slice) Len() int {
	return len(s) * positionsInSlot
}

func (s slice) At(i uint64) bool {
	slot, bitPosition := s.at(i)
	return hasBit(slot, bitPosition)
}

func (s slice) at(i uint64) (uint64, uint64) {
	slotNumber, bitPosition := slotNumberWithBitPosition(i)
	return s[slotNumber], bitPosition
}

func (s slice) Set(i uint64) {
	slotNumber, bitPosition := slotNumberWithBitPosition(i)
	s[slotNumber] = setBit(uint64(s[slotNumber]), bitPosition)
}

func (s slice) Unset(i uint64) {
	slotNumber, bitPosition := slotNumberWithBitPosition(i)
	s[slotNumber] = clearBit(uint64(s[slotNumber]), bitPosition)
}

func slotNumberWithBitPosition(i uint64) (uint64, uint64) {
	slotNumber := i / positionsInSlot
	bitPosition := i - (positionsInSlot * slotNumber)
	return slotNumber, bitPosition
}

// Bitwise OR | = 0011 | 0101 => 0111
func setBit(n uint64, pos uint64) uint64 {
	n |= (1 << pos)
	return n
}

// Bitwise AND & = 0011 & 0101 => 0001
func hasBit(n uint64, pos uint64) bool {
	val := n & (1 << pos)
	return (val > 0)
}

// Clears the bit at pos in n.
// Bitwise NOT ^ = ^0101 => 1010
// Bitwise AND & = 0011 & 0101 => 0001
func clearBit(n uint64, pos uint64) uint64 {
	mask := ^(1 << pos)
	n &= uint64(mask)
	return n
}
